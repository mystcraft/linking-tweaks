package veovismuaddib.linking_tweaks.proxy;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.*;
import veovismuaddib.linking_tweaks.*;
import veovismuaddib.linking_tweaks.listeners.StarFissureDropListener;

@Mod.EventBusSubscriber
public class CommonProxy {
    public Configuration config;

    public void preInit(FMLPreInitializationEvent e) {
    }

    public void init(FMLInitializationEvent e) {}

    public void postInit(FMLPostInitializationEvent e) {
        MinecraftForge.EVENT_BUS.register(new StarFissureDropListener());
    }
}
